﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Penitentia
{
    /// <summary>
    /// Interaction logic for NewMemberWindow.xaml
    /// </summary>
    public partial class NewMemberWindow : Window
    {
        public event EventHandler<AddNewMemberEventArgs> RaiseAddNewMemberEvent;
        private Visibility _electedVisibility = Visibility.Hidden;
        private TextBox[] _textBoxes;
        public NewMemberWindow()
        {
            InitializeComponent();
            _textBoxes = new TextBox[] { Question1TextBox, Question2TextBox, Question3TextBox, Question4TextBox, Question5TextBox, Question6TextBox, NameTextBox, SurnameTextBox, FieldOfStudyTextBox, EmailTextBox, SpecializationTextBox };
            FunctionComboBox.SelectedIndex = 0;
            YearComboBox.SelectedIndex = 0;
            NameTextBox.Focus();
        }

        private void FunctionComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if ((FunctionValues)FunctionComboBox.SelectedValue == FunctionValues.Member)
                _electedVisibility = Visibility.Hidden;
            else
                _electedVisibility = Visibility.Visible;
            ElectedDatePicker.Visibility = _electedVisibility;
            ElectedTextBlock.Visibility = _electedVisibility;
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void AddExitButton_Click(object sender, RoutedEventArgs e)
        {
            if (!checkIfFilled()) return;
            RaiseAddNewMemberEvent(this, new AddNewMemberEventArgs(CreateMember()));
            Close();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            if (!checkIfFilled()) return;
            RaiseAddNewMemberEvent(this, new AddNewMemberEventArgs(CreateMember()));
        }

        private Member CreateMember()
        {
            Member member;
            if ((FunctionValues)FunctionComboBox.SelectedValue == FunctionValues.Member)
                member = new Member();
            else
            {
                member = new ManagementMember();
                ((ManagementMember)member).Elected = ElectedDatePicker.SelectedDate ?? DateTime.Now;
            }

            member.Joined = JoinedDatePicker.SelectedDate ?? DateTime.Now;
            member.Name = NameTextBox.Text;
            member.Surname = SurnameTextBox.Text;
            member.FieldOfStudy = FieldOfStudyTextBox.Text;
            member.Email = EmailTextBox.Text;
            member.Specialization = SpecializationTextBox.Text;
            member.Questions[0] = Question1TextBox.Text;
            member.Questions[1] = Question2TextBox.Text;
            member.Questions[2] = Question3TextBox.Text;
            member.Questions[3] = Question4TextBox.Text;
            member.Questions[4] = Question5TextBox.Text;
            member.Questions[5] = Question6TextBox.Text;

            member.Year = (YearValues)Enum.Parse(typeof(YearValues), YearComboBox.Text);
            member.Function = (FunctionValues)Enum.Parse(typeof(FunctionValues), FunctionComboBox.Text);

            foreach (var element in _textBoxes)
            {
                element.Text = string.Empty;
            }

            NameTextBox.Focus();

            return member;
        }

        private bool checkIfFilled()
        {
            foreach (var element in _textBoxes)
            {
                if (string.IsNullOrWhiteSpace(element.Text))
                {
                    MessageBox.Show("Nie wszystkie pola zostały wypełnione", "Błąd!", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }
            }
            if ((JoinedDatePicker.SelectedDate == null ) || ((FunctionValues)FunctionComboBox.SelectedValue == FunctionValues.Member && ElectedDatePicker.SelectedDate == null))
                if (MessageBox.Show("W przypadku nie wybrania daty zostanie wprowadzona aktualna data.\nCzy chcesz kontynuować?", "Ostrzeżenie!", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    return false;
            return true;
        }
    }

    public class AddNewMemberEventArgs : EventArgs
    {
        public AddNewMemberEventArgs(Member member)
        {
            NewMember = member;
        }
        public Member NewMember { get; private set; }
    }
}
