Projekt na Akademię C#

Prosta aplikacja służąca do przechowywania i zarządzania listą członków koła naukowego Penitentia, w którym działam dorywczo jako administrator.

Cały program składa się z listy członków z możliwością obejrzenia szczegółów oraz edycji każdego z nich.