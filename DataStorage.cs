﻿using System;
using System.IO;
using System.Windows;
using System.Xml.Serialization;

namespace Penitentia
{
    /// <summary>
    /// Serializing and deserializing XML files
    /// </summary>
    static class DataStorage
    {
        public static bool SaveToXML(object data)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter("Members.xml", false))
                {
                    XmlSerializer serializer = new XmlSerializer(data.GetType());
                    serializer.Serialize(writer, data);
                    writer.Close();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
                return false;
            }
            return true;
        }
        public static T LoadFromXML<T>() where T : new()
        {
            T data = new T();
            try
            {
                using (StreamReader reader = new StreamReader("Members.xml"))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    data = (T)serializer.Deserialize(reader);
                    reader.Close();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }
            return data;
        }
    }
}
