﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Penitentia
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool _membersCollectionChanged;
        private ObservableCollection<Member> _members;
        public MainWindow()
        {
            InitializeComponent();
            _members = DataStorage.LoadFromXML<ObservableCollection<Member>>();
            MembersDataGrid.ItemsSource = _members;
        }

        private void MembersDataGrid_SourceUpdated(object sender, System.Windows.Data.DataTransferEventArgs e)
        {
            _membersCollectionChanged = true;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_membersCollectionChanged)
            {
                var result = MessageBox.Show("Zostały wprowadzone zmiany.\nCzy chcesz je zachować?", "Zmiany", MessageBoxButton.YesNoCancel, MessageBoxImage.Exclamation, MessageBoxResult.Cancel);
                if (result == MessageBoxResult.Yes)
                {
                    if (!DataStorage.SaveToXML(_members))
                        e.Cancel = true;
                }
                else if (result == MessageBoxResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (!DataStorage.SaveToXML(_members)) return;
            MessageBox.Show("Zapisywanie przebiegło pomyślnie.", "Zapis.", MessageBoxButton.OK,MessageBoxImage.Information);
            _membersCollectionChanged = false;
        }

        private void Expander_Click(object sender, RoutedEventArgs e)
        {
            for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
                if (vis is DataGridRow)
                {
                    var row = (DataGridRow)vis;
                    row.DetailsVisibility = row.DetailsVisibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
                    break;
                }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Czy na pewno chcesz usunąć zaznaczone elementy?", "Potwierdzenie", MessageBoxButton.YesNo, MessageBoxImage.Exclamation, MessageBoxResult.No) == MessageBoxResult.Yes)
            {
                _membersCollectionChanged = true;
                for (int i = MembersDataGrid.SelectedItems.Count - 1; i >= 0; --i)
                    _members.RemoveAt(MembersDataGrid.Items.IndexOf(MembersDataGrid.SelectedItems[i]));
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            NewMemberWindow newMemberWindow = new NewMemberWindow();
            newMemberWindow.RaiseAddNewMemberEvent += new EventHandler<AddNewMemberEventArgs>(newMemberWindow_RaiseCustomEvent);
            newMemberWindow.Show();
        }

        void newMemberWindow_RaiseCustomEvent(object sender, AddNewMemberEventArgs e)
        {
            _members.Add(e.NewMember);
            //This can impact performance!
            _members.Sort<Member>();
        }
    }
}
