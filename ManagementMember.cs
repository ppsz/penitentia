﻿using System;

namespace Penitentia
{
    public class ManagementMember : Member
    {
        public DateTime Elected { get; set; }

        public ManagementMember() { }
        public ManagementMember(string name, string surname, string fieldOfStudy, string specialization, YearValues year, DateTime joined, FunctionValues function, string email, string[] questions, DateTime elected) : base(name, surname, fieldOfStudy, specialization, year, joined, function, email, questions)
        {
            Elected = elected;
        }

    }
}