﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace Penitentia
{
    public enum YearValues
    {
        nd,
        I,
        II,
        III,
        IV,
        V,
    }

    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum FunctionValues
    {
        [Description("Członek")]
        Member,
        [Description("Przewodniczący")]
        Manager,
        [Description("Vice-przewodniczący")]
        VcManager,
        [Description("Dwuczłon")]
        Consigliere,
        [Description("Sekretarz")]
        Secretary
    }

    [XmlInclude(typeof(Member))]
    [XmlInclude(typeof(ManagementMember))]
    public class Member : IComparable<Member>
    { 
        public string Name { get; set; }
        public string Surname { get; set; }
        public string FieldOfStudy { get; set; }
        public string Specialization { get; set; }
        public YearValues Year { get; set; }
        public DateTime Joined { get; set; }
        public FunctionValues Function { get; set; }
        public string Email { get; set; }
        public string[] Questions { get; set; }
        public int CompareTo(Member other)
        {
            var result = this.Surname.CompareTo(other.Surname);
            if (result == 0)
                return this.Name.CompareTo(other.Name);
            else
                return result;
        }

        public Member() {
            Questions = new string[6];
        }
        public Member(string name, string surname, string fieldOfStudy, string specialization, YearValues year, DateTime joined, FunctionValues function, string email, string[] questions)
        {
            Name = name;
            Surname = surname;
            FieldOfStudy = fieldOfStudy;
            Specialization = specialization;
            Year = year;
            Joined = joined;
            Function = function;
            Email = email;
            Questions = questions;
        }
    }
}